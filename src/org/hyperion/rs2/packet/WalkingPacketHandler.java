package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.pf.AStarPathFinder;
import org.hyperion.rs2.pf.Path;
import org.hyperion.rs2.pf.PathFinder;
import org.hyperion.rs2.pf.Point;
import org.hyperion.rs2.pf.TileMap;
import org.hyperion.rs2.pf.TileMapBuilder;

/**
 * A packet which handles walking requests.
 * @author Graham Edgecombe
 *
 */
public class WalkingPacketHandler implements PacketHandler {

	@Override
	public void handle(Player player, Packet packet) {
		int size = packet.getLength();
		if(packet.getOpcode() == 248) {
		    size -= 14;
		}
		player.setInWalkEvent(false);
		player.resetFollowing();
		player.setInCombat(false);
		player.getWalkingQueue().reset();
		player.getActionQueue().clearNonWalkableActions();
		player.getActionSender().closeInterface();
		player.resetInteractingEntity();

		final int steps = (size - 5) / 2;
		final int[][] path = new int[steps][2];

		final int firstX = packet.getLEShortA();
		for (int i = 0; i < steps; i++) {
		    path[i][0] = packet.getByte();
		    path[i][1] = packet.getByte();
		}
		final int firstY = packet.getLEShort();
		final boolean runSteps = packet.getByteC() == 1;
		
		player.getWalkingQueue().setRunningQueue(runSteps);
		
		/*player.getWalkingQueue().addStep(firstX, firstY );
		
		for (int i = 0; i < steps; i++) {
		    path[i][0] += firstX;
		    path[i][1] += firstY;
		    player.getWalkingQueue().addStep(path[i][0], path[i][1]);
		}
		player.getWalkingQueue().finish();*/

		
		int radius = 32;
		int x = firstX;
		int y = firstY;
		if (steps > 0) {
			x += path[steps-1][0];
			y += path[steps-1][1];
		}

		x -= player.getLocation().getX();
		y -= player.getLocation().getY();
								
		TileMapBuilder bldr = new TileMapBuilder(player.getLocation(), radius);
		TileMap map = bldr.build();
		
		PathFinder pf = new AStarPathFinder();
		Path p = pf.findPath(player.getLocation(), radius, map, radius, radius, x + radius, y + radius);
		if(p == null) return;
								
		player.getWalkingQueue().reset();
		for(Point p2 : p.getPoints()) {
			player.getWalkingQueue().addStep(p2.getX(), p2.getY());
		}
		player.getWalkingQueue().finish();
	}

}
