package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.ObjectManager;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.net.Packet;

public class LoadRegion implements PacketHandler {

	@Override
	public void handle(Player player, Packet packet) {
		ObjectManager.loadObjectsByRegion(player);
	}

}
