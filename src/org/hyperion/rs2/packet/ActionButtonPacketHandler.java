package org.hyperion.rs2.packet;

import org.hyperion.rs2.ScriptManager;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler;
import org.hyperion.rs2.net.Packet;

/**
 * Handles clicking on most buttons in the interface.
 * @author Graham Edgecombe
 *
 */
public class ActionButtonPacketHandler implements PacketHandler {
	
	@Override
	public void handle(Player player, Packet packet) {
		final int button = packet.getShort();
		ScriptManager.getScriptManager().invoke("handleButton", new Object[] {player, button});
		
		CombatStyleHandler.setWeaponHandler(player, button);
	}

}
