package org.hyperion.rs2.packet;

import org.hyperion.rs2.event.impl.WalkToEvent;
import org.hyperion.rs2.model.NPC;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.content.combat.CombatUtils;
import org.hyperion.rs2.model.content.combat.impl.AttackAction;
import org.hyperion.rs2.net.Packet;

public class NpcOptionPackethandler implements PacketHandler {
	
	private static final int NPC_OPTION_1 = 72;
	private static final int NPC_OPTION_2 = 155;
	private static final int NPC_OPTION_3 = 17;

	@Override
	public void handle(Player player, Packet packet) {
				
		switch(packet.getOpcode()) {
			case NPC_OPTION_1:
				handleOptionOne(player, packet);
				break;
			case NPC_OPTION_2:
				handleOptionTwo(player, packet);
				break;
			case NPC_OPTION_3:
				handleOptionThree(player, packet);
				break;
		}		
	}
	
	private void handleOptionOne(Player player, Packet packet) {
		int index = packet.getShortA();
		NPC npc = (NPC) World.getWorld().getNPCs().get(index);
		player.startCombatAction(npc, new AttackAction());
		/*World.getWorld().submit(new WalkToEvent(player, npc.getLocation(), 1) {

			@Override
			public void arrived() {
				player.getActionSender().sendMessage("NPC Click 1: " + index);				
			}
			
			public void failed() {
				player.getActionSender().sendMessage("Could not reach the npc.");
			}			
			
		});*/		
	}
	
	private void handleOptionTwo(Player player, Packet packet) {		
		int index = packet.getLEShort();
		NPC npc = (NPC) World.getWorld().getNPCs().get(index);
		World.getWorld().submit(new WalkToEvent(player, npc.getLocation(), 1) {

			@Override
			public void arrived() {
				player.getActionSender().sendMessage("NPC Click 2: " + index);				
			}
			
			public void failed() {
				player.getActionSender().sendMessage("Could not reach the npc.");
			}			
			
		});		
	}
	
	private void handleOptionThree(Player player, Packet packet) {		
		int index = packet.getLEShortA();
		NPC npc = (NPC) World.getWorld().getNPCs().get(index);
		World.getWorld().submit(new WalkToEvent(player, npc.getLocation(), 1) {

			@Override
			public void arrived() {
				player.getActionSender().sendMessage("NPC Click 3: " + index);				
			}
			
			public void failed() {
				player.getActionSender().sendMessage("Could not reach the npc.");
			}			
			
		});		
	}	
}
