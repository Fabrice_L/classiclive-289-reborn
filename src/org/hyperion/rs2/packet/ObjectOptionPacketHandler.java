package org.hyperion.rs2.packet;

import org.hyperion.rs2.event.impl.WalkToEvent;
import org.hyperion.rs2.model.GameObjectDefinition;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.model.content.mining.Mining;
import org.hyperion.rs2.model.content.mining.ProspectRock;
import org.hyperion.rs2.model.content.mining.Mining.Node;
import org.hyperion.rs2.model.content.woodcutting.Woodcutting;
import org.hyperion.rs2.model.content.woodcutting.Woodcutting.Tree;
import org.hyperion.rs2.net.Packet;

/**
 * Object option packet handler.
 * @author Graham Edgecombe
 *
 */
public class ObjectOptionPacketHandler implements PacketHandler {
	
	/**
	 * Option 1 opcode.
	 */
	private static final int OPTION_1 = 132, OPTION_2 = 252;

	@Override
	public void handle(Player player, Packet packet) {
		switch(packet.getOpcode()) {
		case OPTION_1:
			handleOption1(player, packet);
			break;
		case OPTION_2:
			handleOption2(player, packet);
			break;
		}
	}

	/**
	 * Handles the option 1 packet.
	 * @param player The player.
	 * @param packet The packet.
	 */
	private void handleOption1(Player player, Packet packet) {
		int x = packet.getLEShortA() & 0xFFFF;
		int id = packet.getShort() & 0xFFFF;
		int y = packet.getShortA() & 0xFFFF;
		Location loc = Location.create(x, y, player.getLocation().getZ());
		GameObjectDefinition def = GameObjectDefinition.forId(id);
		World.getWorld().submit(new WalkToEvent(player, loc, def.getSizeX() >= def.getSizeY() ? def.getSizeX() : def.getSizeY()) {

			@Override
			public void arrived() {
				// woodcutting
				Tree tree = Tree.forId(id);
				if(tree != null) {
					player.getActionQueue().addAction(new Woodcutting(player, loc, tree));
				}
				// mining
				Node node = Node.forId(id);
				if(node != null) {
					player.getActionQueue().addAction(new Mining(player, loc, node));
				}
				
				switch(id) {
					case 2852:
						if (x == 2580 && y == 3875) {
							player.setTeleportTarget(Location.create(2808, 10002, 0));
						}
						break;
					case 4500:
						if (x == 2809 && y == 10001) {
							player.setTeleportTarget(Location.create(2584, 3876, 0));
						}
						break;
						
					default:
						System.out.println("object clicked [1]: " + id);
						break;
				}
				
			}
			
			public void failed() {
				player.getActionSender().sendMessage("Could not reach that object");
			}
			
		});
	}
	
    /**
     * Handles the option 2 packet.
     * @param player The player.
     * @param packet The packet.
     */
    private void handleOption2(Player player, Packet packet) {        
        int id = packet.getLEShortA() & 0xFFFF;
        int y = packet.getLEShort() & 0xFFFF;
        int x = packet.getShortA() & 0xFFFF;
        Location loc = Location.create(x, y, player.getLocation().getZ());   
		GameObjectDefinition def = GameObjectDefinition.forId(id);
		
		World.getWorld().submit(new WalkToEvent(player, loc, def.getSizeX() >= def.getSizeY() ? def.getSizeX() : def.getSizeY()) {

			@Override
			public void arrived() {
				//rock prospecting
				Node node = Node.forId(id);
		        if(node != null && player.getLocation().isWithinInteractionDistance(loc)) {
		            player.getActionQueue().addAction(new ProspectRock(player, loc, node));
		            return;
		        }
				if (id == 2213) {
					Bank.open(player);
				}
				System.out.println("object clicked [2]: " + id);
			}
			
			public void failed() {
				player.getActionSender().sendMessage("Could not reach that object");
			}
			
		});
    }


}
