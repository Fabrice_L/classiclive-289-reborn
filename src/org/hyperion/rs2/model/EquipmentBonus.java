package org.hyperion.rs2.model;

public enum EquipmentBonus {
	
	ATT_STAB("Stab"),
	ATT_SLASH("Slash"),
	ATT_CRUSH("Crush"),
	ATT_MAGIC("Magic"),
	ATT_RANGED("Ranged"),
	
	DEF_STAB("Stab"),
	DEF_SLASH("Slash"),
	DEF_CRUSH("Crush"),
	DEF_MAGIC("Magic"),
	DEF_RANGED("Ranged"),
	
	STRENGTH("Strength"),
	PRAYER("Prayer");
	
	private String bonusName;
	
	EquipmentBonus(String bonusName) {
		this.setBonusName(bonusName);
	}

	public String getBonusName() {
		return bonusName;
	}

	private void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}
	
	
}