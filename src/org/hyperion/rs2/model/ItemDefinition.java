package org.hyperion.rs2.model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.logging.Logger;

import org.hyperion.util.Buffers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * The item definition manager.
 * @author Vastico
 * @author Graham Edgecombe
 *
 */
public class ItemDefinition {
	
	/**
	 * Logger instance.
	 */
	private static final Logger logger = Logger.getLogger(ItemDefinition.class.getName());
	
	/**
	 * The definition array.
	 */
	private static ItemDefinition[] definitions;
	
	/**
	 * Gets a definition for the specified id.
	 * @param id The id.
	 * @return The definition.
	 */
	public static ItemDefinition forId(int id) {
		return definitions[id];
	}
	

	public static void init() {
		File file = new File("./data/itemdefs.json");
		try {
	        JsonParser parser = new JsonParser();
            Object obj = parser.parse(new FileReader(file));

            JsonArray items = (JsonArray) obj;
            definitions = new ItemDefinition[items.size()];
            for (JsonElement element : items) {
            	if (element.isJsonNull()) continue;
            	JsonObject item = element.getAsJsonObject();
            	
            	int id = item.get("id").getAsInt();
            	String name = item.get("name").getAsString();
            	String examine = item.get("examine").getAsString();
            	boolean stackable = item.get("stackable").getAsBoolean();
            	boolean noteable = item.get("noteable").getAsBoolean();
            	boolean noted = item.get("noted").getAsBoolean();
            	int notedId = item.get("notedId").getAsInt();
            	int parentId = item.get("parentId").getAsInt();
            	boolean members = item.get("members").getAsBoolean();
            	int value = item.get("value").getAsInt();
				int highAlc = (int) (value * 0.6D);
				int lowAlc = (int) (value * 0.4D);
				
				JsonObject bonusList = item.get("bonusList").getAsJsonObject();
				int[] bonuses = new int[bonusList.entrySet().size()];
				
				for (int bonus = 0; bonus < bonuses.length; bonus++) {
					bonuses[bonus] = bonusList.get(EquipmentBonus.values()[bonus].toString()).getAsInt();
				}

				definitions[id] = new ItemDefinition(id, name, examine, noted, noteable, stackable, parentId, notedId, members, value, highAlc, lowAlc, bonuses);

            }
            logger.info("Loaded " + items.size() + " item definitions");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Id.
	 */
	private final int id;
	
	/**
	 * Name.
	 */
	private final String name;
	
	/**
	 * Description.
	 */
	private final String examine;
	
	/**
	 * Noted flag.
	 */
	private final boolean noted;
	
	/**
	 * Noteable flag.
	 */
	private final boolean noteable;
	
	/**
	 * Stackable flag.
	 */
	private final boolean stackable;
	
	/**
	 * Non-noted id.
	 */
	private final int parentId;
	
	/**
	 * Noted id.
	 */
	private final int notedId;
	
	/**
	 * Members flag.
	 */
	private final boolean members;
	
	/**
	 * Shop value.
	 */
	private final int shopValue;
	
	/**
	 * High alc value.
	 */
	private final int highAlcValue;
	
	/**
	 * Low alc value.
	 */
	private final int lowAlcValue;
	
	private int[] bonus = new int[12];
	
	/**
	 * Creates the item definition.
	 * @param id The id.
	 * @param name The name.
	 * @param examine The description.
	 * @param noted The noted flag.
	 * @param noteable The noteable flag.
	 * @param stackable The stackable flag.
	 * @param parentId The non-noted id.
	 * @param notedId The noted id.
	 * @param members The members flag.
	 * @param shopValue The shop price.
	 * @param highAlcValue The high alc value.
	 * @param lowAlcValue The low alc value.
	 */
	private ItemDefinition(int id, String name, String examine, boolean noted, boolean noteable, boolean stackable, int parentId, int notedId, boolean members, int shopValue, int highAlcValue, int lowAlcValue, int[] bonus) {
		this.id = id;
		this.name = name;
		this.examine = examine;
		this.noted = noted;
		this.noteable = noteable;
		this.stackable = stackable;
		this.parentId = parentId;
		this.notedId = notedId;
		this.members = members;
		this.shopValue = shopValue;
		this.highAlcValue = highAlcValue;
		this.lowAlcValue = lowAlcValue;
		this.bonus = bonus;
	}
	
	/**
	 * Gets the id.
	 * @return The id.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Gets the name.
	 * @return The name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the description.
	 * @return The description.
	 */
	public String getDescription() {
		return examine;
	}
	
	/**
	 * Gets the noted flag.
	 * @return The noted flag.
	 */
	public boolean isNoted() {
		return noted;
	}
	
	/**
	 * Gets the noteable flag.
	 * @return The noteable flag.
	 */
	public boolean isNoteable() {
		return noteable;
	}
	
	/**
	 * Gets the stackable flag.
	 * @return The stackable flag.
	 */
	public boolean isStackable() {
		return stackable || noted;
	}
	
	/**
	 * Gets the normal id.
	 * @return The normal id.
	 */
	public int getNormalId() {
		return parentId;
	}
	
	/**
	 * Gets the noted id.
	 * @return The noted id.
	 */
	public int getNotedId() {
		return notedId;
	}
	
	/**
	 * Gets the members only flag.
	 * @return The members only flag.
	 */
	public boolean isMembersOnly() {
		return members;
	}
	
	/**
	 * Gets the value.
	 * @return The value.
	 */
	public int getValue() {
		return shopValue;
	}
	
	/**
	 * Gets the low alc value.
	 * @return The low alc value.
	 */
	public int getLowAlcValue() {
		return lowAlcValue;
	}
	
	/**
	 * Gets the high alc value.
	 * @return The high alc value.
	 */
	public int getHighAlcValue() {
		return highAlcValue;
	}

	public int[] getBonus() {
		return bonus;
	}

	public void setBonus(int[] bonus) {
		this.bonus = bonus;
	}

}
