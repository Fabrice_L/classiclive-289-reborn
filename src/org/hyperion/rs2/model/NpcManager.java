package org.hyperion.rs2.model;

import java.io.File;
import java.io.FileReader;
import java.util.logging.Logger;

import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;
import org.hyperion.cache.index.impl.StandardIndex;
import org.hyperion.cache.npc.NpcDefinitionListener;
import org.hyperion.cache.npc.NpcDefinitionParser;
import org.hyperion.rs2.util.Misc;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class NpcManager implements NpcDefinitionListener {
	
	private static final Logger logger = Logger.getLogger(ObjectManager.class.getName());
	
	private int npcDefinitions;

	public void loadNpcs() {
		File folder = new File("./data/npcs/locs");
		for (File file : Misc.listFilesForFolder(folder)) {
			try {
		        JsonParser parser = new JsonParser();
	            Object obj = parser.parse(new FileReader(file));

	            JsonObject load = (JsonObject) obj;
	            JsonArray npcs = load.get("npcs").getAsJsonArray();
	            for (JsonElement element : npcs) {
	            	JsonObject npcData = element.getAsJsonObject();
	            	int npcId = npcData.get("npcId").getAsInt();
	                JsonObject position = npcData.get("position").getAsJsonObject();
	                Location pos = Location.create(position.get("posX").getAsInt(), position.get("posY").getAsInt(), position.get("height").getAsInt());
	            	int maxWalk = npcData.get("maxWalk").getAsInt();
	            	NPC npc = new NPC(NPCDefinition.forId(npcId));
	            	npc.setSpawnPosition(Location.create(pos.getX(), pos.getY(), pos.getZ()));
	            	npc.setLocation(pos);
	            	npc.setMaxWalk(maxWalk);
	            	World.getWorld().register(npc);
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loadData() throws InvalidCacheException {
		Cache cache = new Cache(new File("./data/cache/"));

		try {
		StandardIndex[] defIndices = cache.getIndexTable().getNpcDefinitionIndices();
		new NpcDefinitionParser(cache, defIndices, this).parse();
		
		logger.info("Loaded " + npcDefinitions + " npc definitions.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void npcDefinitionParsed(NPCDefinition def) {
		NPCDefinition.addDefinition(def);
		npcDefinitions++;
	}
}
