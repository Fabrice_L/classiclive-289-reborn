package org.hyperion.rs2.model.skillingnode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.Player;

public class SkillingNodeContainer {
	
	private Map<Location, SkillingNode> nodeMap;
	
	public SkillingNodeContainer() {		
		nodeMap = new HashMap<Location, SkillingNode>();
	}
	
	public void addNode(SkillingNode node, Location location, Player player) {
		
		if (nodeMap.containsKey(location)) {
			lookupPlayerInNodeMap(player);
		} else {
			nodeMap.put(location, node);
			lookupPlayerInNodeMap(player);
		}		
	}
	
	private void lookupPlayerInNodeMap(Player player) {
		Iterator<Map.Entry<Location, SkillingNode>> it = nodeMap.entrySet().iterator();
		while (it.hasNext()) {
			if (it.next().getValue().containsPlayer(player)) {
				
			}
		}
	}
	
	public void removeNode(SkillingNode node) {
		
	}

}
