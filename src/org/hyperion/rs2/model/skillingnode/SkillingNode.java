package org.hyperion.rs2.model.skillingnode;

import java.util.HashMap;
import java.util.Map;

import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.Player;

public class SkillingNode {
	
	private Location location;
	
	private Map<Integer, Player> node;
	
	public SkillingNode(int nodeId, Location location) {	
		this.location = location;
		node = new HashMap<Integer, Player>();		
	}
	
	public void addPlayer(Player player) {
		node.put(player.getIndex(), player);
	}
	
	public void removePlayer(Player player) {
		node.remove(player.getIndex());
	}
	
	public boolean containsPlayer(Player player) {
		return node.containsKey(player.getIndex());
	}
	
	public int getSize() {
		return node.size(); 
	}
	
	public Location getLocation() {
		return location;
	}
}
