package org.hyperion.rs2.model.content.combat;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.content.combat.impl.CombatTypeHandler.CombatTypes;

/**
 * This class holds alot of useful methods for combat
 * 
 * @author Fabrice L
 *
 */

public class CombatUtils {

    /**
     * Sets the attacktimer for a specific entity
     * 
     * @param entity
     */
    public static void setAttackTimer(Entity entity) {
	entity.setAttackTimer(entity instanceof Player ? PlayerWeapon.getWeaponTimer(((Player) entity).getEquipment().get(Equipment.SLOT_WEAPON)) : 5);
    }

    /**
     * gets the attacktimer of a specific entity
     * 
     * @param entity
     * @return timer
     */
    public static int getAttackTimer(Entity entity) {
	return entity.getAttackTimer();
    }

    /**
     * Decrease a specific entity's attacktimer by 1
     * 
     * @param entity
     */
    public static void decreaseAttackTimer(Entity entity) {
    	entity.setAttackTimer(entity.getAttackTimer() - 1);
    }

    /**
     * returns the emote of the attacking entity
     * 
     * @param entity
     * @return emote
     */
    public static int getAttackEmote(Entity entity) {
    	return entity instanceof Player ? PlayerWeapon.getWeaponEmote(((Player) entity).getEquipment().get(Equipment.SLOT_WEAPON)) : 422;
    }

    /**
     * Returns the combattype a specific entity is using
     * 
     * @param entity
     * @return combattype
     */
    public static CombatTypes getCombatType(Entity entity) {
    	String style = entity.getCombatStyle().toString().toLowerCase();
		if (style.toString().contains("magic")) {
			return CombatTypes.MAGIC;
		} else if (style.toString().contains("ranged")) {
			return CombatTypes.RANGED;
		} else {
			return CombatTypes.MELEE;
		}
    }

    public static int getCombatDistance(Entity entity) {
		switch (getCombatType(entity)) {
			case RANGED:
			    return 8;
			case MAGIC:
			    return 10;
			case MELEE:
			    return 1;
		}
		return 1;
    }

}
