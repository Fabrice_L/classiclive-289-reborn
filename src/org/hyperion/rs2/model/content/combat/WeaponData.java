package org.hyperion.rs2.model.content.combat;

import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.AxeHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.BowHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.HalberdHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.HammerHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.MaceHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.PickaxeHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.SpearHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.SwordsHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.ThrowableHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.WandHandler;
import org.hyperion.rs2.model.content.combat.impl.handlers.UnarmedHandler;

public enum WeaponData {

    UNARAMED(new String[] { "unarmed" }, 5855, new UnarmedHandler()), BOW(
	    new String[] { "bow" }, 1764, new BowHandler()), WAND(
	    new String[] { "wand" }, 328, new WandHandler()), THROWABLE(
	    new String[] { "dart", "knife", "javelin", "thrownaxe" }, 4446,
	    new ThrowableHandler()), SWORDS(new String[] { "dagger", "sword",
	    "scimitar" }, 2276, new SwordsHandler()), PICKAXE(
	    new String[] { "pickaxe" }, 5570, new PickaxeHandler()), AXES(
	    new String[] { " axe", "battleaxe" }, 1698, new AxeHandler()), HALBERD(
	    new String[] { "halberd" }, 8460, new HalberdHandler()), SPEAR(
	    new String[] { "spear" }, 4679, new SpearHandler()), MACE(
	    new String[] { "mace" }, 3796, new MaceHandler()), HAMMERS(
	    new String[] { "warhammer", "maul" }, 425, new HammerHandler());

    private String name[];
    private int combatInterface;
    private CombatStyleHandler style;

    WeaponData(String[] name, int combatInterface, CombatStyleHandler style) {
	this.name = name;
	this.combatInterface = combatInterface;
	this.style = style;
    }

    public String getNames()[] {
	return name;
    }

    public int getInterface() {
	return combatInterface;
    }

    public CombatStyleHandler getStyle() {
	return style;
    }
}
