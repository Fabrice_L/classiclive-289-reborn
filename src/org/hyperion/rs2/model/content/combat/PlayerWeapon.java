package org.hyperion.rs2.model.content.combat;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.ItemDefinition;

/**
 * Holds everything related to the players weaponry
 * 
 * @author Fabrice L
 *
 */

public class PlayerWeapon {

    /**
     * Returns the weapon timer
     * 
     * @param item
     * @return timer
     */
    public static int getWeaponTimer(Item item) {
    if (item != null) {
		switch (item.getId()) {
		case 4151:
		    return 8;
		}
    }
	return 5;
    }

    /**
     * returns the weapon emote
     * 
     * @param item
     * @return emote
     */
    public static int getWeaponEmote(Item item) {
    	if (item != null) {
		switch (item.getId()) {
	
		}
    	}
    	return 422;
    }

    /**
     * Returns whether the player weapon is a ranged weapon or not
     * 
     * @param item
     * @return isranging
     */
    public static boolean isRangedWeapon(Item item) {
    	if (item != null) {
	    	ItemDefinition def = ItemDefinition.forId(item.getId());
			String itemName = def.getName().toLowerCase();
			if (itemName.contains("longbow") || itemName.contains("shortbow")) {
				return true;
			}
    	}
		return false;
    }

}
