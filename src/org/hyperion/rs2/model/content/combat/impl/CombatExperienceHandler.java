package org.hyperion.rs2.model.content.combat.impl;

import org.hyperion.rs2.model.Entity;

/**
 * 
 * @author Fabrice L
 *
 */

public abstract class CombatExperienceHandler {
    /**
     * gives the attacker the required experience
     * 
     * @param attacker
     *            experience
     */
    public abstract void appendExperience(Entity attacker, int hit);

}
