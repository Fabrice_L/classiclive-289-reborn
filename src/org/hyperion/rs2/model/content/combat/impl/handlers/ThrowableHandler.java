package org.hyperion.rs2.model.content.combat.impl.handlers;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler;

public class ThrowableHandler extends CombatStyleHandler {

    @Override
    public void handleWeaponInterface(Entity entity, int buttonId) {
    	switch(buttonId) {
	    	case 4454:
	    		entity.setCombatStyle(CombatStyle.ACCURATE_RANGED);
	    		break;
	    	case 4453:
	    		entity.setCombatStyle(CombatStyle.RAPID_RANGED);
	    		break;
	    	case 4452:
	    		entity.setCombatStyle(CombatStyle.LONGRANGE_RANGED);
	    		break;
    	}
    }

	@Override
	public void updateWeaponStyle(Entity entity) {
		Player player = (Player) entity;
		switch(player.getCombatStyle()) {
			case ACCURATE_MELEE:
			case CONTROLLED_MELEE:
			case ACCURATE_RANGED:
			case JUST_MAGIC:
				player.setCombatStyle(CombatStyle.ACCURATE_RANGED);
				player.getActionSender().sendConfig(43, 0);
				break;
			case DEFENSIVE_MELEE:
			case DEFENSIVE_MAGIC:
			case LONGRANGE_RANGED:
				player.setCombatStyle(CombatStyle.LONGRANGE_RANGED);
				player.getActionSender().sendConfig(43, 2);
				break;
			case AGGRESSIVE_MELEE:
			case RAPID_RANGED:
				player.setCombatStyle(CombatStyle.RAPID_RANGED);
				player.getActionSender().sendConfig(43, 1);
				break;
		
		}		
	}

}
