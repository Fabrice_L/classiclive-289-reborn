package org.hyperion.rs2.model.content.combat.impl;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.ItemDefinition;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.content.combat.WeaponData;

public abstract class CombatStyleHandler {

    /**
     * Returns the weapon interface
     * 
     * @param item
     */
    public abstract void handleWeaponInterface(Entity entity, int buttonId);
    public abstract void updateWeaponStyle(Entity entity);

    public static void setWeaponHandler(Entity entity, int buttonId) {
		Item item = ((Player) entity).getEquipment().get(Equipment.SLOT_WEAPON);
		for (WeaponData weaponData : WeaponData.values()) {
		    for (int i = 0; i < weaponData.getNames().length; i++) {
		    	String itemName = "unarmed";
		    	if (item != null) {
		    		ItemDefinition def = ItemDefinition.forId(item.getId());
		    		itemName = def.getName().toLowerCase();
		    	}
				if (itemName.contains(weaponData.getNames()[i])) {
				    startInterfaceHandler(entity, buttonId, weaponData.getStyle());
				}
		    }
		}
    }
    
    public static void setWeaponStyle(Entity entity) {
		Item item = ((Player) entity).getEquipment().get(Equipment.SLOT_WEAPON);
		for (WeaponData weaponData : WeaponData.values()) {
		    for (int i = 0; i < weaponData.getNames().length; i++) {
		    	String itemName = "unarmed";
		    	if (item != null) {
		    		ItemDefinition def = ItemDefinition.forId(item.getId());
		    		itemName = def.getName().toLowerCase();
		    	}
				if (itemName.contains(weaponData.getNames()[i])) {
					System.out.println(weaponData.getStyle());
				    startWeaponStyleHandler(entity, weaponData.getStyle());
				}
		    }
		}
    }
    
    private static void startWeaponStyleHandler(Entity entity, CombatStyleHandler handler) {
    	handler.updateWeaponStyle(entity);
    }

    private static void startInterfaceHandler(Entity entity, int buttonId, CombatStyleHandler handler) {
    	handler.handleWeaponInterface(entity, buttonId);
    }
    
    /**
     * holds all the possible attackstyles
     * 
     * @author Fabrice L
     *
     */
    public enum CombatStyle {
    	ACCURATE_MELEE, AGGRESSIVE_MELEE, DEFENSIVE_MELEE, CONTROLLED_MELEE, ACCURATE_RANGED, RAPID_RANGED, LONGRANGE_RANGED, JUST_MAGIC, DEFENSIVE_MAGIC
    }
    
    public enum AttackStyle {
    	STAB, SLASH, CRUSH, RANGED, MAGIC;
    }
}
