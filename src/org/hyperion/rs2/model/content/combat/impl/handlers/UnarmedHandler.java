package org.hyperion.rs2.model.content.combat.impl.handlers;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler;

public class UnarmedHandler extends CombatStyleHandler {

    @Override
    public void handleWeaponInterface(Entity entity, int buttonId) {
		switch(buttonId) {
		case 5860:
			entity.setCombatStyle(CombatStyle.ACCURATE_MELEE);
			break;
		case 5862:
			entity.setCombatStyle(CombatStyle.AGGRESSIVE_MELEE);
			break;
		case 5861:
			entity.setCombatStyle(CombatStyle.DEFENSIVE_MELEE);
			break;
		}
    }

	@Override
	public void updateWeaponStyle(Entity entity) {
		Player player = (Player) entity;
		switch(player.getCombatStyle()) {
			case ACCURATE_MELEE:
			case CONTROLLED_MELEE:
			case ACCURATE_RANGED:
			case JUST_MAGIC:
				player.setCombatStyle(CombatStyle.ACCURATE_MELEE);
				player.getActionSender().sendConfig(43, 0);
				break;
			case DEFENSIVE_MELEE:
			case DEFENSIVE_MAGIC:
			case LONGRANGE_RANGED:
				player.setCombatStyle(CombatStyle.DEFENSIVE_MELEE);
				player.getActionSender().sendConfig(43, 2);
				break;
			case AGGRESSIVE_MELEE:
			case RAPID_RANGED:
				player.setCombatStyle(CombatStyle.AGGRESSIVE_MELEE);
				player.getActionSender().sendConfig(43, 1);
				break;
		
		}
	}

}
