package org.hyperion.rs2.model.content.combat.impl;

import org.hyperion.rs2.event.Event;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.content.combat.CombatAction;
import org.hyperion.rs2.model.content.combat.CombatUtils;
import org.hyperion.rs2.model.content.combat.impl.CombatTypeHandler.CombatTypes;
import org.hyperion.rs2.model.content.combat.impl.types.MagicAction;
import org.hyperion.rs2.model.content.combat.impl.types.MeleeAction;
import org.hyperion.rs2.model.content.combat.impl.types.RangedAction;

/**
 * @author Fabrice L
 *
 *         Represents a single tile based combat action.
 */
public final class AttackAction extends CombatAction {

    @Override
    public void performCombatAction(Entity attacker, Entity victim) {

	attacker.setInCombat(true);
	attacker.setInteractingEntity(victim);
	attacker.setAttackTimer(0);
	attacker.setFollowing(victim, CombatUtils.getCombatDistance(attacker));
	attacker.setLastCombatAction(System.currentTimeMillis());
	World.getWorld().submit(new Event(600) {

		@Override
		public void execute() {
			if (!attacker.isInCombat() || attacker.isDead() || attacker == null || victim == null || victim.isDead() 
					|| attacker.getLastCombatAction() + 20000 < System.currentTimeMillis() && !attacker.isFollowing()) {
				resetCombat(attacker);
				System.out.println("combat reset");
				stop();
			}
			if (attacker instanceof Player) {
				if (!((Player) attacker).isOnline()) {
					resetCombat(victim);
					stop();
				}
			}
			if (victim instanceof Player) {
				if (!((Player) victim).isOnline()) {
					resetCombat(attacker);
					stop();
				}
			}
			if (CombatUtils.getAttackTimer(attacker) > 0) {
			    CombatUtils.decreaseAttackTimer(attacker);
			    return;
			}
			if (attacker.isFollowing()) {
				attacker.setLastCombatAction(System.currentTimeMillis());
			}
			if (!attacker.getLocation().withinDistance(victim.getLocation(), CombatUtils.getCombatDistance(attacker))) {
				if (!attacker.isFollowing() && attacker.isInCombat() && attacker instanceof Player) {
					attacker.setFollowing(victim, CombatUtils.getCombatDistance(attacker));
				}
				return;
			}
			
			if (!victim.isInCombat() && (victim instanceof Player ? victim.isAutoRetaliating() : true)) {
			    performCombatAction(victim, attacker);
			    victim.setAttackTimer(2);
			}
			
			CombatUtils.setAttackTimer(attacker);
			attacker.playAnimation(Animation.create(CombatUtils.getAttackEmote(attacker)));
			attacker.face(victim.getLocation());
			if (CombatUtils.getCombatType(attacker).equals(
				CombatTypes.MELEE)) {
			    startAction(attacker, victim, new MeleeAction());
			}
			if (CombatUtils.getCombatType(attacker).equals(
				CombatTypes.RANGED)) {
			    startAction(attacker, victim, new RangedAction());
			}
			if (CombatUtils.getCombatType(attacker).equals(
				CombatTypes.MAGIC)) {
			    startAction(attacker, victim, new MagicAction());
			}
			
		}
		
	});
    }
    
    public static void resetCombat(Entity entity) {
    	Entity victim = entity.getInteractingEntity();
    	entity.setInCombat(false);
    	entity.resetFollowing();
    	entity.resetInteractingEntity();
    	if (victim != null) {
    		victim.setInCombat(false);
    		victim.resetFollowing();
    		victim.resetInteractingEntity();
    	}
    }

    public static final void startAction(final Entity attacker, final Entity victim, final CombatTypeHandler action) {
    	action.initiateAction(attacker, victim);
    }
}
