package org.hyperion.rs2.model.content.combat.impl.experience;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.content.combat.impl.CombatExperienceHandler;

public class DefensiveMelee extends CombatExperienceHandler {

    @Override
    public void appendExperience(Entity attacker, int hit) {
    	Player player = (Player) attacker;
    	player.getSkills().addExperience(Skills.DEFENCE, 4 * hit);
    	player.getSkills().addExperience(Skills.HITPOINTS, 1.33 * hit);
    }

}
