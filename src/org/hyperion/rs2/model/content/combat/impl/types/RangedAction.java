package org.hyperion.rs2.model.content.combat.impl.types;

import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatExperienceHandler;
import org.hyperion.rs2.model.content.combat.impl.CombatTypeHandler;
import org.hyperion.rs2.model.content.combat.impl.experience.AccurateRanged;
import org.hyperion.rs2.model.content.combat.impl.experience.LongRanged;
import org.hyperion.rs2.model.content.combat.impl.experience.RapidRanged;
import org.hyperion.rs2.util.Misc;

public class RangedAction extends CombatTypeHandler {

    /**
     * Initiates the ranged combat action
     * 
     * @param attacker
     * @param victim
     */
    public void initiateAction(Entity attacker, Entity victim) {
		getMaxHit(attacker);
		attacker.getWalkingQueue().reset();
		if (getHitRoll(attacker, victim)) {
		    victim.inflictDamage(attacker.getHitDiff(), attacker.getHitDiff() == 0 ? HitType.NO_DAMAGE : HitType.NORMAL_DAMAGE);
		    if (attacker instanceof Player) {
		    	appendExperience(attacker);
		    }
		} else {
			victim.inflictDamage(0, HitType.NO_DAMAGE);
		}
    }

    /**
     * Returns the Max hit of the attacking entity
     * 
     * @param attacker
     * @return maxhit
     */
    public void getMaxHit(Entity attacker) {
		int maxHit = 1;
		attacker.setHitDiff(Misc.random(maxHit));
    }

    /**
     * Returns whether the victim is being hit by the attacker or not
     * 
     * @param attacker
     * @param victim
     * @return victim has been hit?
     */
    public boolean getHitRoll(Entity attacker, Entity victim) {
	return true;
    }

    @Override
    public void appendExperience(Entity attacker) {
    	CombatExperienceHandler handler = null;
    	switch(attacker.getCombatStyle()) {
		case ACCURATE_RANGED:
			handler = new AccurateRanged();
			break;
		case LONGRANGE_RANGED:
			handler = new LongRanged();
			break;
		case RAPID_RANGED:
			handler = new RapidRanged();
			break;
		default:
			System.out.println("wrong combat style in RangedAction: " + attacker.getCombatStyle());
			break;
    	
    	}
    	if (handler != null) {
    		handler.appendExperience(attacker, attacker.getHitDiff());
    	}
    }

}
