package org.hyperion.rs2.model.content.combat.impl;

import org.hyperion.rs2.model.Entity;

public abstract class CombatTypeHandler {

    /**
     * initiates the combat action
     * 
     * @param attacker
     * @param victim
     */
    public abstract void initiateAction(Entity attacker, Entity victim);

    /**
     * Gets the maxhit of the attacker
     * 
     * @param attacker
     * @return maxhit
     */
    public abstract void getMaxHit(Entity attacker);

    /**
     * Returns whether the victim should be hit or not
     * 
     * @param attacker
     * @param victim
     * @return victim is hit?
     */
    public abstract boolean getHitRoll(Entity attacker, Entity victim);

    /**
     * Returns the experience the attacker should get
     * 
     * @param attacker
     * @param victim
     * @param style
     */
    public abstract void appendExperience(final Entity attacker);

    /**
     * This enum holds all the different combatTypes
     * 
     * @author Fabrice L
     *
     */
    public enum CombatTypes {
	MELEE, RANGED, MAGIC
    }
}
