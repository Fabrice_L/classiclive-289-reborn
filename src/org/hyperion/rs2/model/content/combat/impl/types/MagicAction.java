package org.hyperion.rs2.model.content.combat.impl.types;

import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatExperienceHandler;
import org.hyperion.rs2.model.content.combat.impl.CombatTypeHandler;
import org.hyperion.rs2.model.content.combat.impl.experience.DefensiveMagic;
import org.hyperion.rs2.model.content.combat.impl.experience.JustMagic;
import org.hyperion.rs2.util.Misc;

public class MagicAction extends CombatTypeHandler {

    /**
     * Initiates the Magic Combat action
     * 
     * @param attacker
     * @param victim
     */
    public void initiateAction(Entity attacker, Entity victim) {
		getMaxHit(attacker);
		attacker.getWalkingQueue().reset();
		if (getHitRoll(attacker, victim)) {
		    victim.inflictDamage(attacker.getHitDiff(), attacker.getHitDiff() == 0 ? HitType.NO_DAMAGE : HitType.NORMAL_DAMAGE);
		    if (attacker instanceof Player) {
		    	appendExperience(attacker);
		    }
		} else {
			victim.inflictDamage(0, HitType.NO_DAMAGE);
		}
    }

    /**
     * Returns the Max hit of the attacking entity
     * 
     * @param attacker
     * @return maxhit
     */
    public void getMaxHit(Entity attacker) {
		int maxHit = 1;
		attacker.setHitDiff(Misc.random(maxHit));
    }

    /**
     * Returns whether the victim is being hit by the attacker or not
     * 
     * @param attacker
     * @param victim
     * @return victim has been hit?
     */
    public boolean getHitRoll(Entity attacker, Entity victim) {
	return true;
    }

    /**
     * Returns the experience the attacker should get
     */
    @Override
    public void appendExperience(Entity attacker) {
    	CombatExperienceHandler handler = null;
    	switch(attacker.getCombatStyle()) {
		case DEFENSIVE_MAGIC:
			handler = new DefensiveMagic();
			break;
		case JUST_MAGIC:
			handler = new JustMagic();
			break;
		default:
			System.out.println("wrong combat style in MagicAction: " + attacker.getCombatStyle());
			break;
    	
    	}
    	if (handler != null) {
    		handler.appendExperience(attacker, attacker.getHitDiff());
    	}
    }

}
