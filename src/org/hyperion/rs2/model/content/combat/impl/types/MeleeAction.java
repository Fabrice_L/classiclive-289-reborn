package org.hyperion.rs2.model.content.combat.impl.types;

import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.EquipmentBonus;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.content.combat.impl.CombatExperienceHandler;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler.AttackStyle;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler.CombatStyle;
import org.hyperion.rs2.model.content.combat.impl.CombatTypeHandler;
import org.hyperion.rs2.model.content.combat.impl.experience.AccurateMelee;
import org.hyperion.rs2.model.content.combat.impl.experience.AggressiveMelee;
import org.hyperion.rs2.model.content.combat.impl.experience.ControlledMelee;
import org.hyperion.rs2.model.content.combat.impl.experience.DefensiveMelee;
import org.hyperion.rs2.util.Misc;

public class MeleeAction extends CombatTypeHandler {

    /**
     * Initiates the melee action
     * 
     * @param attacker
     * @param victim
     */
    public void initiateAction(Entity attacker, Entity victim) {
		getMaxHit(attacker);
		attacker.getWalkingQueue().reset();
		if (getHitRoll(attacker, victim)) {
		    victim.inflictDamage(attacker.getHitDiff(), attacker.getHitDiff() == 0 ? HitType.NO_DAMAGE : HitType.NORMAL_DAMAGE);
		    if (attacker instanceof Player) {
		    	appendExperience(attacker);
		    }
		} else {
			victim.inflictDamage(0, HitType.NO_DAMAGE);
		}
    }

    /**
     * Returns the Max hit of the attacking entity
     * 
     * @param attacker
     * @return maxhit
     */
    public void getMaxHit(Entity attacker) {
    	double maxHit;
    	if (attacker instanceof Player) {
    		Player player = (Player) attacker;
    		maxHit = 0.5 + getEffectiveHitLevel(attacker) * (player.getEquipmentBonus()[EquipmentBonus.STRENGTH.ordinal()] + 64)/ 640;
        	System.out.println("Maxhit is: " + maxHit);
    	} else {
    		maxHit = 1;
    	}
    	attacker.setHitDiff(Misc.random((int) Math.round(maxHit)));
    }

    private int getEffectiveHitLevel(Entity attacker) {
		double lvl = ((Player) attacker).getSkills().getLevel(Skills.STRENGTH);
		lvl *= getHitPrayerBonus(attacker);
		lvl = Math.floor(lvl);
		lvl += getHitStanceBonus(attacker.getCombatStyle());
		lvl += 8;
       	return (int) Math.floor(lvl);
    }
    
    private int getHitPrayerBonus(Entity attacker) {
    	return 1;
    }
    
    private int getHitStanceBonus(CombatStyle style) {
    	if (style == CombatStyle.CONTROLLED_MELEE) {
    		return 1;
    	} else if (style == CombatStyle.AGGRESSIVE_MELEE) {
    		return 3;
    	}
    	return 0;
    }
    
    /**
     * Returns whether the victim is being hit by the attacker or not
     * 
     * @param attacker
     * @param victim
     * @return victim has been hit?
     */
    public boolean getHitRoll(Entity attacker, Entity victim) {
    	boolean hitVictim = false;
    	double attRoll = getEffectiveAccuracyLevel(attacker, false) + (getEquipmentBonus(attacker, false, attacker.getAttackStyle()) + 64);
    	double defRoll = getEffectiveAccuracyLevel(victim, true) + (getEquipmentBonus(victim, true, attacker.getAttackStyle())+ 64);
    	double accuracy = 0;
    	if (attRoll > defRoll) {
    		accuracy = 1 - (defRoll + 2) / (2 * (attRoll + 1));
    	} else {
    		accuracy = attRoll / ( 2 * (defRoll + 1));
    	}
    	hitVictim = Math.random() < accuracy;
    	System.out.println("Accuracy: " + accuracy * 100);
    	return hitVictim;
    }
    
    private double getEquipmentBonus(Entity entity, boolean victim, AttackStyle attackStyle) {
    	int bonus = 0;
    	
    	if (attackStyle == AttackStyle.SLASH) {
    		if (entity instanceof Player) {
    			Player player = (Player) entity;
    			if (victim) {
    				return player.getEquipmentBonus()[EquipmentBonus.DEF_SLASH.ordinal()];
    			} else {
    				return player.getEquipmentBonus()[EquipmentBonus.ATT_SLASH.ordinal()];
    			}
    		} else {
    			//Todo: Add bonuses to npcs
    		}
    	} else if (attackStyle == AttackStyle.STAB) {
    		if (entity instanceof Player) {
    			Player player = (Player) entity;
    			if (victim) {
    				return player.getEquipmentBonus()[EquipmentBonus.DEF_STAB.ordinal()];
    			} else {
    				return player.getEquipmentBonus()[EquipmentBonus.ATT_STAB.ordinal()];
    			}
    		} else {
    			//Todo: Add bonuses to npcs
    		}
    	} else if (attackStyle == AttackStyle.CRUSH) {
    		if (entity instanceof Player) {
    			Player player = (Player) entity;
    			if (victim) {
    				return player.getEquipmentBonus()[EquipmentBonus.DEF_CRUSH.ordinal()];
    			} else {
    				return player.getEquipmentBonus()[EquipmentBonus.ATT_CRUSH.ordinal()];
    			}
    		} else {
    			//Todo: Add bonuses to npcs
    		}
    	}
    	return bonus;
    }
    
    private double getEffectiveAccuracyLevel(Entity entity, boolean victim) {
    	double lvl = 0;
    	if (entity instanceof Player) {
    		Player player = (Player) entity;
    		if (victim) {
    			lvl = player.getSkills().getLevel(Skills.DEFENCE);
    			lvl *= getAccuracyPrayerBonus(player, true);
    			lvl = Math.floor(lvl);
    			lvl += getAccuracyStanceBonus(player.getCombatStyle(), true);
    			lvl += 8;
    		} else {
    			lvl = player.getSkills().getLevel(Skills.ATTACK);
    			lvl *= getAccuracyPrayerBonus(player, false);
    			lvl = Math.floor(lvl);
    			lvl += getAccuracyStanceBonus(player.getCombatStyle(), false);
    			lvl += 8;
    		}
    	}
    	return Math.floor(lvl);
    }
    
    private int getAccuracyStanceBonus(CombatStyle style, boolean victim) {
    	if (victim) {
    		if (style == CombatStyle.CONTROLLED_MELEE) {
        		return 1;
        	} else if (style == CombatStyle.DEFENSIVE_MELEE) {
        		return 3;
        	}
    	} else {
    		if (style == CombatStyle.CONTROLLED_MELEE) {
        		return 1;
        	} else if (style == CombatStyle.AGGRESSIVE_MELEE) {
        		return 3;
        	}
    	}
    	return 0;
    }
    
    private int getAccuracyPrayerBonus(Entity entity, boolean victim) {
    	return 1;
    }

    @Override
    public void appendExperience(Entity attacker) {
    	CombatExperienceHandler handler = null;
    	switch(attacker.getCombatStyle()) {
			case ACCURATE_MELEE:
				handler = new AccurateMelee();
				break;
			case AGGRESSIVE_MELEE:
				handler = new AggressiveMelee();
				break;
			case DEFENSIVE_MELEE:
				handler = new DefensiveMelee();
				break;
			case CONTROLLED_MELEE:
				handler = new ControlledMelee();
				break;
			default:
				System.out.println("wrong combat style in MeleeAction: " + attacker.getCombatStyle());
				break;
    	
    	}
    	if (handler != null) {
    		handler.appendExperience(attacker, attacker.getHitDiff());
    	}
    }

}
