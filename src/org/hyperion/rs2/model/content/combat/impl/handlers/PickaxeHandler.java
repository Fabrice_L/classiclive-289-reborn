package org.hyperion.rs2.model.content.combat.impl.handlers;

import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Player;
import org.hyperion.rs2.model.content.combat.impl.CombatStyleHandler;

public class PickaxeHandler extends CombatStyleHandler {

    @Override
    public void handleWeaponInterface(Entity entity, int buttonId) {
    	switch (buttonId) {
    		case 5576:
    			entity.setCombatStyle(CombatStyle.ACCURATE_MELEE);
    			break;
    		case 5579:
    			entity.setCombatStyle(CombatStyle.AGGRESSIVE_MELEE);
    			break;
    		case 5578:
    			entity.setCombatStyle(CombatStyle.AGGRESSIVE_MELEE);
    			break;
    		case 5577:
    			entity.setCombatStyle(CombatStyle.DEFENSIVE_MELEE);
    			break;
    	}
    }

	@Override
	public void updateWeaponStyle(Entity entity) {
		Player player = (Player) entity;
		switch(player.getCombatStyle()) {
			case ACCURATE_MELEE:
			case ACCURATE_RANGED:
			case CONTROLLED_MELEE:
			case JUST_MAGIC:
				player.setCombatStyle(CombatStyle.ACCURATE_MELEE);
				player.getActionSender().sendConfig(43, 0);
				break;
			case DEFENSIVE_MELEE:
			case DEFENSIVE_MAGIC:
			case LONGRANGE_RANGED:
				player.setCombatStyle(CombatStyle.DEFENSIVE_MELEE);
				player.getActionSender().sendConfig(43, 3);
				break;
			case AGGRESSIVE_MELEE:
			case RAPID_RANGED:
				player.setCombatStyle(CombatStyle.AGGRESSIVE_MELEE);
				player.getActionSender().sendConfig(43, 1);
				break;
		
		}		
	}

}
