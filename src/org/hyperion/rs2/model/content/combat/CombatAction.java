package org.hyperion.rs2.model.content.combat;

import org.hyperion.rs2.model.Entity;

/**
 * @author Dylan Vicchiarelli
 *
 *         Represents a possible combat action.
 */
public abstract class CombatAction {

    /**
     * Executes the combat action of interest.
     * 
     * @param attacker
     *            The entity performing the combat action.
     * 
     * @param victim
     *            The victim of the combat action.
     */
    public abstract void performCombatAction(Entity attacker, Entity victim);
}