package org.hyperion.rs2.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;
import org.hyperion.cache.index.impl.MapIndex;
import org.hyperion.cache.index.impl.StandardIndex;
import org.hyperion.cache.map.LandscapeListener;
import org.hyperion.cache.map.LandscapeParser;
import org.hyperion.cache.map.MapListener;
import org.hyperion.cache.map.MapParser;
import org.hyperion.cache.obj.ObjectDefinitionListener;
import org.hyperion.cache.obj.ObjectDefinitionParser;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.pf.Tile;
import org.hyperion.rs2.util.Misc;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Manages all of the in-game objects.
 * @author Graham Edgecombe
 *
 */
public class ObjectManager implements LandscapeListener, ObjectDefinitionListener, MapListener {
	
	/**
	 * Logger instance.
	 */
	private static final Logger logger = Logger.getLogger(ObjectManager.class.getName());
	
	/**
	 * The number of definitions loaded.
	 */
	private int definitionCount = 0;
	
	/**
	 * The count of objects loaded.
	 */
	private int objectCount = 0;
	private int tileCount = 0;
	
	/**
	 * Loads the objects in the map.
	 * @throws IOException if an I/O error occurs.
	 * @throws InvalidCacheException if the cache is invalid.
	 */
	public void load() throws IOException, InvalidCacheException {
		Cache cache = new Cache(new File("./data/cache/"));
		try {
			logger.info("Loading definitions...");
			StandardIndex[] defIndices = cache.getIndexTable().getObjectDefinitionIndices();
			new ObjectDefinitionParser(cache, defIndices, this).parse();
			logger.info("Loaded " + definitionCount + " object definitions.");
			logger.info("Loading map...");
			MapIndex[] mapIndices = cache.getIndexTable().getMapIndices();
			for(MapIndex index : mapIndices) {
				new LandscapeParser(cache, index.getIdentifier(), this).parse();
				new MapParser(cache, index.getIdentifier(), this).parse();
			}
			logger.info("Loaded " + objectCount + " objects.");
			logger.info("Loaded " + tileCount + " tiles.");
		} finally {
			cache.close();
		}
	}
	
	public void loadCustomObjects() {
		File folder = new File("./data/objects/add");
		for (File file : Misc.listFilesForFolder(folder)) {
			try {
		        JsonParser parser = new JsonParser();
	            Object obj = parser.parse(new FileReader(file));

	            JsonObject load = (JsonObject) obj;
	            JsonArray objects = load.get("objects").getAsJsonArray();
	            for (JsonElement element : objects) {
	            	JsonObject object = element.getAsJsonObject();
	            	int objId = object.get("objectId").getAsInt();
	                JsonObject position = object.get("position").getAsJsonObject();
	                Location pos = Location.create(position.get("posX").getAsInt(), position.get("posY").getAsInt(), position.get("height").getAsInt());
	            	int type = object.get("type").getAsInt();
	            	int face = object.get("face").getAsInt();
	                GameObjectDefinition objDef = GameObjectDefinition.forId(objId);
	            	GameObject gameObj = new GameObject(objDef, pos, type, face);
	        		World.getWorld().getRegionManager().getRegionByLocation(gameObj.getLocation()).getGameObjects().add(gameObj);
	        		World.getWorld().getRegionManager().getRegionByLocation(gameObj.getLocation()).getCustomGameObjects().add(gameObj);
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loadRemovedObjects() {
		File folder = new File("./data/objects/remove");
		for (File file : Misc.listFilesForFolder(folder)) {
			try {
		        JsonParser parser = new JsonParser();
	            Object obj = parser.parse(new FileReader(file));

	            JsonObject load = (JsonObject) obj;
	            JsonArray objects = load.get("objects").getAsJsonArray();
	            for (JsonElement element : objects) {
	            	JsonObject object = element.getAsJsonObject();
	            	int objId = object.get("objectId").getAsInt();
	                JsonObject position = object.get("position").getAsJsonObject();
	                Location pos = Location.create(position.get("posX").getAsInt(), position.get("posY").getAsInt(), position.get("height").getAsInt());
	    			Iterator<GameObject> it = World.getWorld().getRegionManager().getRegionByLocation(pos).getGameObjects().iterator();
	    		    while (it.hasNext()) {    
	    		        GameObject removeObject = (GameObject) it.next();
	    		        if (removeObject.getLocation().equals(pos) && (removeObject.getDefinition().getId() == objId || objId == -1)) {
	    	        		World.getWorld().getRegionManager().getRegionByLocation(removeObject.getLocation()).getRemovedObjects().add(removeObject);
	    		        	//it.remove();
	    		        }
	    		    }
	        		//World.getWorld().getRegionManager().getRegionByLocation(gameObj.getLocation()).getGameObjects().add(gameObj);
	        		//World.getWorld().getRegionManager().getRegionByLocation(gameObj.getLocation()).getCustomGameObjects().add(gameObj);
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void loadObjectsByRegion(Player player) {
		for (Region region : World.getWorld().getRegionManager().getSurroundingRegions(player.getLocation())) {
			for (GameObject object : region.getCustomGameObjects()) {
				player.getActionSender().sendCreateObject(object.getDefinition().getId(), object.getType(), object.getRotation(), object.getLocation());
			}
			for (GameObject object : region.getRemovedObjects()) {
				player.getActionSender().removeObject(-1, 0, 10, object.getLocation());
			}
		}

	}

	@Override
	public void objectParsed(GameObject obj) {
		objectCount++;
		World.getWorld().getRegionManager().getRegionByLocation(obj.getLocation()).getGameObjects().add(obj);
	}

	@Override
	public void objectDefinitionParsed(GameObjectDefinition def) {
		definitionCount++;
		GameObjectDefinition.addDefinition(def);
	}
	
	public static ArrayList<Integer> getObjectTypesOnLocation(GameObject object) {
		ArrayList<Integer> objectsOnSpot = new ArrayList<Integer>();
		for (GameObject objects : World.getWorld().getRegionManager().getRegionByLocation(object.getLocation()).getGameObjects()) {
			objectsOnSpot.add(objects.getType());
		}
		return objectsOnSpot;
	}
	
	@Override
	public void tileParsed(Tile tile) {
		tileCount++;
		if ((tile.getWalkable() & 0x1)==0x1) { // no walking for this tile
		    World.getWorld().getRegionManager().getRegionByLocation(tile.getLoc()).getTiles().put(tile.getLoc(), false);
		} else if ((tile.getWalkable() & 0x2)==0x2) { // bridge, so we set tile below to hasBridgeAbove
		    if (tile.getLoc().getZ() > 0) { // this is the deepest layer, we cant go below
		    	Location pos = Location.create(tile.getLoc().getX(), tile.getLoc().getY(), 0);
			    World.getWorld().getRegionManager().getRegionByLocation(tile.getLoc()).getTiles().put(pos, true);
		    }
		}
	}
}
