package org.hyperion.rs2.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <p>Represents a type of NPC.</p>
 * @author Graham Edgecombe
 *
 */
public class NPCDefinition {
	
	public static final int MAX_DEFINITIONS = 2291;
	
	private static NPCDefinition[] definitions = new NPCDefinition[MAX_DEFINITIONS];
	
	
	/**
	 * Adds a definition. TODO better way?
	 * @param def The definition.
	 */
	static void addDefinition(NPCDefinition def) {
		definitions[def.getId()] = def;
	}

	
	/**
	 * Gets an npc definition by its id.
	 * @param id The id.
	 * @return The definition.
	 */
	public static NPCDefinition forId(int id) {
		return definitions[id];
	}
	
	/**
	 * The id.
	 */
	private int id;
	
	/**
	 * The name;
	 */
	private String name;
	
	/**
	 * The description
	 */
	private String description;
	
	/**
	 * The combatLevel
	 */
	private int combatLevel;
	
	/**
	 * the npc size
	 */
	private int size;
	
	/**
	 * the animations
	 */
	private int standAnim, walkAnim, walkBackAnim, walkLeftAnim, walkRightAnim;
	
	/**
	 * The npc interactions
	 */
	private final String[] interactions = new String[5];

	
	/**
	 * Creates the definition.
	 * @param id The id.
	 * @param desc 
	 * @param name 
	 */
	public NPCDefinition(int id, String name, String desc, int combatLevel, int size, int standAnim, int walkAnim, int walkAnimOther, int walkBackAnim, int walkLeftAnim, int walkRightAnim, HashMap<Integer, String> actions) {
		this.id = id;
		this.name = name;
		this.description = desc;
		this.combatLevel = combatLevel;
		this.size = size;
		this.standAnim = standAnim;
		this.walkAnim = walkAnim;
		this.setWalkAnimations(walkAnimOther, walkBackAnim, walkLeftAnim, walkRightAnim);
		
		Iterator<Entry<Integer, String>> it = actions.entrySet().iterator();
	    while (it.hasNext()) {
	        @SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry)it.next();
	        interactions[(int) pair.getKey()]  = (String) pair.getValue();
	    }
	}
	
	/**
	 * Gets the id.
	 * @return The id.
	 */
	public int getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getCombatLevel() {
		return combatLevel;
	}

	public int getSize() {
		return size;
	}

	public int getStandAnim() {
		return standAnim;
	}

	public int getWalkAnim() {
		return walkAnim;
	}

	public int getWalkBackAnim() {
		return walkBackAnim;
	}

	public int getWalkLeftAnim() {
		return walkLeftAnim;
	}

	public int getWalkRightAnim() {
		return walkRightAnim;
	}
		
	public void setWalkAnimations(int walkAnim, int walkBackAnim, int walkLeftAnim, int walkRightAnim) {
		this.walkAnim = walkAnim;
		this.walkBackAnim = walkBackAnim;
		this.walkLeftAnim = walkLeftAnim;
		this.walkRightAnim = walkRightAnim;
	}

	public String[] getInteractions() {
		return interactions;
	}
	
}
