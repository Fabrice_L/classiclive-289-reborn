package org.hyperion.rs2.model;

import org.hyperion.rs2.event.impl.DeathEvent;
import org.hyperion.rs2.model.Damage.Hit;
import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.model.region.Region;

/**
 * <p>Represents a non-player character in the in-game world.</p>
 * @author Graham Edgecombe
 *
 */
public class NPC extends Entity {
	
	private int maxWalk;
	private Location spawnPosition;
	private boolean walkingHome;
	private int respawnTimer  = 10;
	
	private int currentHitpoints = 100;
	private int hitPoints = 100;
	/**
	 * The definition.
	 */
	private final NPCDefinition definition;
	
	/**
	 * Creates the NPC with the specified definition.
	 * @param definition The definition.
	 */
	public NPC(NPCDefinition definition) {
		super();
		this.definition = definition;
	}
	
	/**
	 * Gets the NPC definition.
	 * @return The NPC definition.
	 */
	public NPCDefinition getDefinition() {
		return definition;
	}

	@Override
	public void addToRegion(Region region) {
		region.addNpc(this);
	}

	@Override
	public void removeFromRegion(Region region) {
		region.removeNpc(this);
	}

	@Override
	public int getClientIndex() {
		return this.getIndex();
	}

	@Override
	public void inflictDamage(int damage, HitType type) {
		if (damage > currentHitpoints) {
			damage = currentHitpoints;
		}
		Hit hit = new Hit(damage, type);
		if(!getUpdateFlags().get(UpdateFlag.HIT)) {
			getDamage().setHit1(hit);
			getUpdateFlags().flag(UpdateFlag.HIT);
		} else {
			if(!getUpdateFlags().get(UpdateFlag.HIT_2)) {
				getDamage().setHit2(hit);
				getUpdateFlags().flag(UpdateFlag.HIT_2);
			}
		}	
		currentHitpoints -= damage;
		
		if(currentHitpoints <= 0) {
			if(!this.isDead()) {
				playAnimation(Animation.create(836));
				World.getWorld().submit(new DeathEvent(this, 3500));
			}
			this.setDead(true);
		}
	}

	public int getMaxWalk() {
		return maxWalk;
	}

	public void setMaxWalk(int maxWalk) {
		this.maxWalk = maxWalk;
	}

	public Location getSpawnPosition() {
		return spawnPosition;
	}

	public void setSpawnPosition(Location spawnPosition) {
		this.spawnPosition = spawnPosition;
	}

	public boolean isWalkingHome() {
		return walkingHome;
	}

	public void setWalkingHome(boolean walkingHome) {
		this.walkingHome = walkingHome;
	}

	public int getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}

	public int getCurrentHitpoints() {
		return currentHitpoints;
	}

	public void setCurrentHitpoints(int currentHitpoints) {
		this.currentHitpoints = currentHitpoints;
	}

	public int getRespawnTimer() {
		return respawnTimer;
	}

	public void setRespawnTimer(int respawnTimer) {
		this.respawnTimer = respawnTimer;
	}

}
