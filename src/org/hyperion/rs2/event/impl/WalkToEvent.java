package org.hyperion.rs2.event.impl;

import org.hyperion.rs2.event.Event;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Location;

public abstract class WalkToEvent extends Event {

	private Entity entity;
	private Location location;
	private int radius;
	private Location lastLocation;
	private int failRuns = 5;
	
	public WalkToEvent(Entity entity, Location location, int radius) {
		super(600);
		this.entity = entity;
		this.location = location;
		this.radius = radius;
		entity.setInWalkEvent(true);
	}

	@Override
	public void execute() {
		if(entity.getLocation().withinDistance(location, radius)) {
				arrived();
		}
		else if (failRuns == 0 || !entity.getInWalkEvent()) {
			failed();
		}
		else {
			if (lastLocation == entity.getLocation()) {
				System.out.println("failed run");
				failRuns--;
				return;
			}
			lastLocation = entity.getLocation();
			return;
		}
		entity.setInWalkEvent(false);
		this.stop();
	}

	public abstract void arrived();
	
	public void failed() { }
}
