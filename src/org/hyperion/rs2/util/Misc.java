package org.hyperion.rs2.util;

import java.io.File;
import java.util.ArrayList;

public class Misc {
	
	public static int random(int range) {
		int number = (int) (Math.random() * (range + 1));
		return number < 0 ? 0 : number;
	}
	
	public static int random(int min, int max)
	{
		int number = min + (int)(Math.random() * ((max - min) + 1));
		return number;
	}

	public static ArrayList<File> listFilesForFolder(final File folder) {
		ArrayList<File> array = new ArrayList<File>();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	            array.add(fileEntry);
	        }
	    }
		return array;
	}

}
