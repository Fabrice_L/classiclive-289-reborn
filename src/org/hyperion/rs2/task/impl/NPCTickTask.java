package org.hyperion.rs2.task.impl;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.model.NPC;
import org.hyperion.rs2.model.content.combat.CombatUtils;
import org.hyperion.rs2.pf.AStarPathFinder;
import org.hyperion.rs2.pf.Path;
import org.hyperion.rs2.pf.PathFinder;
import org.hyperion.rs2.pf.Point;
import org.hyperion.rs2.pf.TileMap;
import org.hyperion.rs2.pf.TileMapBuilder;
import org.hyperion.rs2.task.Task;
import org.hyperion.rs2.util.Misc;

/**
 * A task which performs pre-update tasks for an NPC.
 * @author Graham Edgecombe
 *
 */
public class NPCTickTask implements Task {
	
	/**
	 * The npc who we are performing pre-update tasks for.
	 */
	private NPC npc;
	
	/**
	 * Creates the tick task.
	 * @param npc The npc.
	 */
	public NPCTickTask(NPC npc) {
		this.npc = npc;
	}

	@Override
	public void execute(GameEngine context) {
		/*
		 * If the map region changed set the last known region.
		 */
		if(npc.isMapRegionChanging()) {
			npc.setLastKnownRegion(npc.getLocation());
		}
		
		/*
		 * Process the next movement in the NPC's walking queue.
		 */
		npc.getWalkingQueue().processNextMovement();
		
		if (!npc.getLocation().withinDistance(npc.getSpawnPosition(), npc.getMaxWalk() > 10 ? npc.getMaxWalk() : 10) && !npc.isWalkingHome()) {
			npc.resetFollowing();
			npc.getWalkingQueue().reset();
			npc.setWalkingHome(true);
			int radius = 16;

			int x = npc.getSpawnPosition().getX() - npc.getLocation().getX() + radius;
			int y = npc.getSpawnPosition().getY() - npc.getLocation().getY() + radius;
			
			boolean canWalk = true;
			
			TileMapBuilder bldr = new TileMapBuilder(npc.getLocation(), radius);
			TileMap map = bldr.build();

			PathFinder pf = new AStarPathFinder();
			Path p = pf.findPath(npc.getLocation(), radius, map,
					radius, radius, x, y);

			if (p == null)
				canWalk = false;
			
			if(canWalk) {
				npc.getWalkingQueue().reset();
				for (Point p2 : p.getPoints()) {
					npc.getWalkingQueue().addStep(p2.getX(),
							p2.getY());
				}
				npc.getWalkingQueue().finish();
			}
		}
		if (npc.getLocation().withinDistance(npc.getSpawnPosition(), 0)) {
			npc.setWalkingHome(false);
			if (npc.isInCombat() && !npc.isFollowing() && npc.getInteractingEntity().getLocation().withinDistance(npc.getSpawnPosition(), npc.getMaxWalk() > 10 ? npc.getMaxWalk() : 10)) {
				npc.setFollowing(npc.getInteractingEntity(), CombatUtils.getCombatDistance(npc));
			}
		}

		
		if (Math.random() < 0.2 && npc.getWalkingQueue().isEmpty() && !npc.isInteracting() && !npc.isWalkingHome()) {

			int radius = 16;

			int maxX = npc.getSpawnPosition().getX() + npc.getMaxWalk();
			int minX = npc.getSpawnPosition().getX() - npc.getMaxWalk();
			int x = Misc.random(minX, maxX) - npc.getLocation().getX() + radius;
			
			int maxY = npc.getSpawnPosition().getY() + npc.getMaxWalk();
			int minY = npc.getSpawnPosition().getY() - npc.getMaxWalk();
			int y = Misc.random(minY, maxY) - npc.getLocation().getY() + radius;
			
			boolean canWalk = true;
			
			TileMapBuilder bldr = new TileMapBuilder(npc.getLocation(), radius);
			TileMap map = bldr.build();

			PathFinder pf = new AStarPathFinder();
			Path p = pf.findPath(npc.getLocation(), radius, map,
					radius, radius, x, y);

			if (p == null)
				canWalk = false;
			
			if(canWalk) {
				npc.getWalkingQueue().reset();
				for (Point p2 : p.getPoints()) {
					npc.getWalkingQueue().addStep(p2.getX(),
							p2.getY());
				}
				npc.getWalkingQueue().finish();
			}
		}
	}

}
