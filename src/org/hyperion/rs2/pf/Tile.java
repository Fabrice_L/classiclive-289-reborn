package org.hyperion.rs2.pf;

import java.util.ArrayList;

import org.hyperion.rs2.model.Location;

/**
 * An individual tile on a <code>TileMap</code>. Immutable.
 * @author Graham Edgecombe
 *
 */
public class Tile {
	
	/**
	 * Constant values used by the bitmask.
	 */
	public static final int NORTH_TRAVERSAL_PERMITTED = 1,
		EAST_TRAVERSAL_PERMITTED = 2,
		SOUTH_TRAVERSAL_PERMITTED = 4,
		WEST_TRAVERSAL_PERMITTED = 8;
	
	public static final int NORTHEAST = 0x4;

	
	private ArrayList<Flag> flagList = new ArrayList<>();
	
	/**
	 * A bitmask which determines which directions can be traversed.
	 */
	private int traversalMask;
	private int walkable;
	private Location loc;
	
	public Tile(ArrayList<Flag> flagList) {
		this.flagList = flagList;
	}
	
	public Tile(Location loc, int flags) {
		this.setLoc(loc);
		this.setWalkable(flags);
		// TODO Auto-generated constructor stub
	}

	public Tile(String string) {
		if (string.equalsIgnoreCase("solid")) {
			flagList.add(Flag.NORTH);
			flagList.add(Flag.EAST);
			flagList.add(Flag.SOUTH);
			flagList.add(Flag.WEST);
			flagList.add(Flag.NORTHWEST);
			flagList.add(Flag.NORTHEAST);
			flagList.add(Flag.SOUTHWEST);
			flagList.add(Flag.SOUTHEAST);
		} else {
			flagList.clear();
		}
	}

	/**
	 * Gets the traversal bitmask.
	 * @return The traversal bitmask.
	 */
	public int getTraversalMask() {
		return traversalMask;
	}
	
	
	public ArrayList<Flag> getFlags() {
		return flagList;
	}
	
	/**
	 * Checks if northern traversal is permitted.
	 * @return True if so, false if not.
	 */
	public boolean isNorthernTraversalPermitted() {
		return !flagList.contains(Flag.NORTH);
	}
	
	/**
	 * Checks if eastern traversal is permitted.
	 * @return True if so, false if not.
	 */
	public boolean isEasternTraversalPermitted() {
		return !flagList.contains(Flag.EAST);
	}
	
	public boolean isNWTraversalPermitted() {
		return !flagList.contains(Flag.NORTHWEST);
	}
	
	public boolean isNETraversalPermitted() {
		return !flagList.contains(Flag.NORTHEAST);
	}
	
	public boolean isSWTraversalPermitted() {
		return !flagList.contains(Flag.SOUTHWEST);
	}
	
	public boolean isSETraversalPermitted() {
		return !flagList.contains(Flag.SOUTHEAST);
	}
	
	/**
	 * Checks if southern traversal is permitted.
	 * @return True if so, false if not.
	 */
	public boolean isSouthernTraversalPermitted() {
		return !flagList.contains(Flag.SOUTH);
	}
	
	/**
	 * Checks if western traversal is permitted.
	 * @return True if so, false if not.
	 */
	public boolean isWesternTraversalPermitted() {
		return !flagList.contains(Flag.WEST)/*(traversalMask & WEST_TRAVERSAL_PERMITTED) > 0*/;
	}

	public int getWalkable() {
		return walkable;
	}

	public void setWalkable(int walkable) {
		this.walkable = walkable;
	}

	public Location getLoc() {
		return loc;
	}

	public void setLoc(Location loc) {
		this.loc = loc;
	}
	
	public enum Flag {
		NORTH,
		EAST,
		SOUTH,
		WEST,
		NORTHWEST,
		NORTHEAST,
		SOUTHWEST,
		SOUTHEAST,
		EAS;
	}
	
}
