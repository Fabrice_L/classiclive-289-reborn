package org.hyperion.rs2.pf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.model.region.RegionManager;
import org.hyperion.rs2.pf.Tile.Flag;

/**
 * A class which assist in building <code>TileMap</code>s from a collection of
 * <code>GameObject</code>s.
 * @author Graham Edgecombe
 *
 */
/**
 * A class which assist in building <code>TileMap</code>s from a collection of
 * <code>GameObject</code>s.
 * 
 * @author Core
 */
public class TileMapBuilder
{

	/**
	 * The tile map being built.
	 */
	private final TileMap tileMap;

	/**
	 * The center position.
	 */
	private final Location centerPosition;

	/**
	 * The radius.
	 */
	private final int radius;


	/**
	 * Sets up the tile map builder with the specified radius, and center
	 * position.
	 * 
	 * @param position
	 *            The center position.
	 * @param radius
	 *            The radius.
	 */
	public TileMapBuilder( Location position, int radius )
	{
		centerPosition = position;
		tileMap = new TileMap( radius * 2 + 1, radius * 2 + 1 );
		this.radius = radius;
	}


	/**
	 * Builds the tile map.
	 * 
	 * @return The built tile map.
	 */
	public TileMap build()
	{
		// the region manager
		RegionManager mgr = World.getWorld().getRegionManager();
		// a set of regions covered by our center position + radius
		Set<Region> coveredRegions = new HashSet<Region>();

		// populates the set of covered regions
		for( int x = - radius - 1; x <= radius + 1; x ++ ) {
			for( int y = - radius - 1; y <= radius + 1; y ++ ) {
				Location loc = centerPosition.transform( x, y, 0 );
				coveredRegions.add( mgr.getRegionByLocation( loc ) );
			}
		}

		// calculate top left positions
		int topX = centerPosition.getX() - radius;
		int topY = centerPosition.getY() - radius;

		// now fills in the tile map
		for( Region region: coveredRegions ) {
			for( GameObject obj: region.getGameObjects() ) {

				if( ! obj.getDefinition().isSolid() ) {
					continue;
				}

				Location loc = obj.getLocation();
				if( loc.getZ() != centerPosition.getZ() ) {
					continue;
				}

				int sizeX = obj.getDefinition().getSizeX();
				int sizeY = obj.getDefinition().getSizeY();
				// position in the tile map
				int posX = loc.getX() - topX;
				int posY = loc.getY() - topY;

				if( posX + sizeX < 0 || posY + sizeY < 0 || posX >= tileMap.getWidth() || posY >= tileMap.getHeight() ) {
					continue;
				}
				 if (obj.getType() == 10 || obj.getType() == 11) {
					if( obj.getRotation() == 1 || obj.getRotation() == 3 ) {
						// switch sizes if rotated
						int temp = sizeX;
						sizeX = sizeY;
						sizeY = temp;
					}
					// world objects
					for (int offX = 0; offX < sizeX; offX++) {
						for (int offY = 0; offY < sizeY; offY++) {
							final int x = offX + posX;
							final int y = offY + posY;
							if (x >= 0 && y >= 0 && x < tileMap.getWidth() && y < tileMap.getHeight())
								tileMap.setTile(x, y, TileMap.SOLID_TILE);
						}
					}
				}
				else if (obj.getType() == 22) {
					// floor decoration
					if (obj.getDefinition().hasActions())
						if (posX >= 0 && posY >= 0 && posX < tileMap.getWidth() && posY < tileMap.getHeight())
							tileMap.setTile(posX, posY, TileMap.SOLID_TILE);
				} 
				else if (obj.getType() >= 0 && obj.getType() <= 3) {
					// walls
					if (posX >= 0 && posY >= 0 && posX < tileMap.getWidth() && posY < tileMap.getHeight()) {
						final int finalRotation = obj.getRotation();
						ArrayList <Flag> flagList = new ArrayList<>();
						// clear flags
						if (obj.getType() == 0) {
							if (finalRotation == 0) {
								flagList.add(Flag.WEST);
							}
							else if (finalRotation == 1) {
								flagList.add(Flag.NORTH);
							}
							else if (finalRotation == 2) {
								flagList.add(Flag.EAST);
							}
							else {
								flagList.add(Flag.SOUTH);
							}

						} else if (obj.getType() == 1 || obj.getType() == 3) {
							if (finalRotation == 0) {
								//flagList.add(Flag.NORTH);
								//flagList.add(Flag.WEST);
								flagList.add(Flag.NORTHWEST);
							} else if (finalRotation == 1) {
								//flagList.add(Flag.NORTH);
								//flagList.add(Flag.EAST);
								flagList.add(Flag.NORTHEAST);
							} else if (finalRotation == 2) {
								//flagList.add(Flag.SOUTH);
								//flagList.add(Flag.EAST);
								flagList.add(Flag.SOUTHEAST);
							} else {
								//flagList.add(Flag.SOUTH);
								//flagList.add(Flag.WEST);
								flagList.add(Flag.SOUTHWEST);
							}
						} else {
							if (finalRotation == 0) {
								flagList.add(Flag.NORTH);
								flagList.add(Flag.WEST);
								//flagList.add(Flag.NORTHWEST);
							} else if (finalRotation == 1) {
								flagList.add(Flag.NORTH);
								flagList.add(Flag.EAST);
								//flagList.add(Flag.NORTHEAST);
							} else if (finalRotation == 2) {
								flagList.add(Flag.SOUTH);
								flagList.add(Flag.EAST);
								//flagList.add(Flag.SOUTHEAST);
							} else {
								flagList.add(Flag.SOUTH);
								flagList.add(Flag.WEST);
								//flagList.add(Flag.SOUTHWEST);
							}
						}
						if (tileMap.getTile(posX, posY).getFlags().isEmpty()) {
							tileMap.setTile(posX, posY, new Tile(flagList));
						} else {
							tileMap.getTile(posX, posY).getFlags().addAll(flagList);
						}


					}
				} else if (obj.getType() == 9) {
					// diagonal walls
					if (posX >= 0 && posY >= 0 && posX < tileMap.getWidth() && posY < tileMap.getHeight())
						tileMap.setTile(posX, posY, TileMap.SOLID_TILE);
				} else {
					// others
				}
			}
			
		    for (GameObject obj : region.getRemovedObjects()) {
		    	
				if( ! obj.getDefinition().isSolid() ) {
					continue;
				}
				
				Location loc = obj.getLocation();
				if( loc.getZ() != centerPosition.getZ() ) {
					continue;
				}
				
				int sizeX = obj.getDefinition().getSizeX();
				int sizeY = obj.getDefinition().getSizeY();
				// position in the tile map
				int posX = loc.getX() - topX;
				int posY = loc.getY() - topY;
				if( obj.getRotation() == 1 || obj.getRotation() == 3 ) {
					// switch sizes if rotated
					int temp = sizeX;
					sizeX = sizeY;
					sizeY = temp;
				}

				if( posX + sizeX < 0 || posY + sizeY < 0 || posX >= tileMap.getWidth() || posY >= tileMap.getHeight() ) {
					continue;
				}
				
				for (int offX = 0; offX < sizeX; offX++) {
					for (int offY = 0; offY < sizeY; offY++) {
						final int x = offX + posX;
						final int y = offY + posY;
						if (x >= 0 && y >= 0 && x < tileMap.getWidth() && y < tileMap.getHeight())
							tileMap.setTile(x, y, TileMap.EMPTY_TILE);
					}
				}
		    }
		    
			Iterator<Entry<Location, Boolean>> tiles = region.getTiles().entrySet().iterator();
		    while (tiles.hasNext()) {
		        @SuppressWarnings("rawtypes")
				Map.Entry pair = (Map.Entry)tiles.next();
		        boolean walkAble = (boolean) pair.getValue();
		        if (!walkAble) {
					int posX = ((Location) pair.getKey()).getX() - topX;
					int posY = ((Location) pair.getKey()).getY() - topY;
					if( posX >= 0 && posY >= 0 && posX < tileMap.getWidth() && posY < tileMap.getHeight() ) {
						tileMap.setTile( posX, posY, TileMap.SOLID_TILE );
					}
		        } else {
					int posX = ((Location) pair.getKey()).getX() - topX;
					int posY = ((Location) pair.getKey()).getY() - topY;
					if( posX >= 0 && posY >= 0 && posX < tileMap.getWidth() && posY < tileMap.getHeight() ) {
						tileMap.setTile( posX, posY, TileMap.EMPTY_TILE );
					}
		        }
		    }
		}

		return tileMap;
	}

}

