package org.hyperion.cache.map;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.hyperion.cache.Cache;
import org.hyperion.cache.index.impl.MapIndex;
import org.hyperion.cache.util.ZipUtils;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.pf.Tile;

/**
 * A class which parses map files in the game cache.
 * @author Graham Edgecombe
 *
 */
/**
 * A class which parses map files in the game cache.
 */
public class MapParser {
 
    /**
    * The cache.
    */
    private Cache cache;
 
    /**
    * The area id.
    */
    private int area;
 
    /**
    * The map listener.
    */
    private MapListener listener;
 
    /**
    * Creates the map parser.
    * @param cache The cache.
    * @param area The area id.
    * @param listener The listener.
    */
    public MapParser(Cache cache, int area, MapListener listener) {
        this.cache = cache;
        this.area = area;
        this.listener = listener;
    }
 
    /**
    * Parses the map file.
    * @throws IOException
    */
    public void parse() throws IOException {
        int x = ((area >> 8) & 0xFF) * 64;
        int y = (area & 0xFF) * 64;

        MapIndex index = cache.getIndexTable().getMapIndex(area);
        ByteBuffer buffer = ZipUtils.unzip(cache.getFile(4, index.getMapFile()));

        for (int plane = 0; plane < 4; plane++) {
            for (int localX = 0; localX < 64; localX++) {
                for (int localY = 0; localY < 64; localY++) {
                    Location loc = Location.create(x + localX, y + localY, plane);
                    int flags = 0;

                    for (;;) {
                        int attributeId = buffer.get() & 0xFF;

                        if (attributeId == 0) {
                            listener.tileParsed(new Tile(loc, flags));
                            break;
                        } else if (attributeId == 1) {
                            buffer.get(); // don't know what this is
                            listener.tileParsed(new Tile(loc, flags));
                            break;
                        } else if (attributeId <= 49) {
                            buffer.get(); // don't know what this is
                        } else if (attributeId <= 81) {
                            flags = attributeId - 49;
                        }
                    }
                }
            }
        }
    }
}