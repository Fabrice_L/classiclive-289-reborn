package org.hyperion.cache.npc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import org.hyperion.cache.Archive;
import org.hyperion.cache.Cache;
import org.hyperion.cache.index.impl.StandardIndex;
import org.hyperion.cache.util.ByteBufferUtils;
import org.hyperion.rs2.model.NPCDefinition;

/**
 * A class which parses object definitions in the game cache.
 * @author Graham Edgecombe
 *
 */
public class NpcDefinitionParser {
	
	/**
	 * The cache.
	 */
	private Cache cache;
	
	/**
	 * The index.
	 */
	private StandardIndex[] indices;
	
	/**
	 * The listener.
	 */
	private NpcDefinitionListener listener;
	
	/**
	 * Creates the object definition parser.
	 * @param cache The cache.
	 * @param indices The indices in the cache.
	 * @param listener The object definition listener.
	 */
	public NpcDefinitionParser(Cache cache, StandardIndex[] indices, NpcDefinitionListener listener) {
		this.cache = cache;
		this.indices = indices;
		this.listener = listener;
	}
	
	/**
	 * Parses the object definitions in the cache.
	 * @throws IOException if an I/O error occurs.
	 */
	public void parse() throws IOException {
		ByteBuffer buf = new Archive(cache.getFile(0, 2)).getFileAsByteBuffer("npc.dat");
		
		for(StandardIndex index : indices) {
			int id = index.getIdentifier();
			int offset = index.getFile(); // bad naming, should be getOffset()
			buf.position(offset);
			
			// TODO read the object definition now
				
			String name = "null";
			String desc = "null";
			int combatLevel = -1;
			int size = 1;
			int standAnim = -1;
			int walkAnim = -1;
			int walkAnimOther = -1;
			int walkBackAnim = -1;
			int walkLeftAnim = -1;
			int walkRightAnim = -1;
			HashMap<Integer, String> actions = new HashMap<>();
				
	outer_loop:
			do {
				int configCode;
				do {
					configCode = buf.get() & 0xFF;
					if(configCode == 0) {
						break outer_loop;
					}
					switch(configCode) {
					case 1:
						int length = buf.get() & 0xFF;
						int[] models = new int[length];
						for (int i = 0; i < length; i++) {
							models[i] = buf.getShort();
						}
						break;
					case 2:
						name = ByteBufferUtils.getString(buf);
						break;
					case 3:
						desc = ByteBufferUtils.getString(buf);
						break;
					case 12:
						size = buf.get();
						break;
					case 13:
						standAnim = buf.getShort();
						break;
					case 14:
						walkAnim = buf.getShort();
						break;
					case 17:
						walkAnimOther = buf.getShort();
						walkBackAnim = buf.getShort();
						walkLeftAnim = buf.getShort();
						walkRightAnim = buf.getShort();
						break;
						
					case 30:
					case 31:
					case 32:
					case 33:
					case 34:
					case 35:
					case 36:
					case 37:
					case 38:
					case 39:
						String action = ByteBufferUtils.getString(buf);
						if (action.equals("hidden")) {
							action = null;
						}			
						actions.put(configCode - 30, action);	
						break;
						
					case 40:
						length = buf.get() & 0xFF;
						int[] originalColours = new int[length];
						int[] replacementColours = new int[length];

						for (int i = 0; i < length; i++) {
							originalColours[i] = buf.getShort();
							replacementColours[i] = buf.getShort();
						}
						break;
						
					case 60:
						length = buf.get() & 0xFF;
						int[] additionalModels = new int[length];

						for (int i = 0; i < length; i++) {
							additionalModels[i] = buf.getShort();
						}
						break;
						
					case 90:
					case 91:
					case 92:
						buf.getShort();
						break;
						
					case 95:
						combatLevel = buf.getShort();
						break;
						
					case 97:
					case 98:
						buf.getShort();
						break;
						
					case 100:
					case 101:
						buf.get();
						break;
						
					case 102:
					case 103:
						buf.getShort();
						break;
						
					case 106:
						wrap(buf.getShort());
						wrap(buf.getShort());
						
						int count = buf.get() & 0xFF;
						int[] morphisms = new int[count + 1];
						Arrays.setAll(morphisms, i -> wrap(buf.getShort()));
						break;


					}

				} while(configCode != 108);
			} while(true);
			
			listener.npcDefinitionParsed(new NPCDefinition(id, name, desc, combatLevel, size, standAnim, walkAnim, walkAnimOther, walkBackAnim, walkLeftAnim, walkRightAnim, actions));
		}
	}
	
	private int wrap(int value) {
		return value == 65_535 ? -1 : value;
	}

}
