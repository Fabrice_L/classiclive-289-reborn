package org.hyperion.cache.npc;

import org.hyperion.rs2.model.NPCDefinition;

/**
 * An object definition listener, which is notified when object definitions
 * have been parsed.
 * @author Graham Edgecombe
 *
 */
public interface NpcDefinitionListener {
	
	/**
	 * Called when an object definition is parsed.
	 * @param def The definition that was parsed.
	 */
	public void npcDefinitionParsed(NPCDefinition def);

}
